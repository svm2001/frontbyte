$(document).ready(function () {
  $(".b-header__burger").click(function (event) {
    $(
      ".b-header__burger, .b-container," +
        " .b-subtitle--header, .b-header-title," +
        " .b-logo__link, .b-header--background, .b-header__body-content, .b-suptitle," +
        " .b-subtitle--team-header, .b-mob-menu-link--about, .b-header__body-top, .b-button--header, .b-mob-menu, .b-text--event-header, .b-mob-menu-link--nowrap"
    ).toggleClass("active");
    $(
      ".b-logo__icon-mobdark, .b-logo__icon-moblight, .b-text--home-header"
    ).toggleClass("active");
    $("body").toggleClass("lock");
  });
});

$(document).ready(function () {
  $(".b-slider__image-block").slick({
    dots: true,
    adaptiveHeight: true,
    infinite: false,
    speed: 1000,
    cssEase: "linear",
    LazyLoad: "ondemand",

    asNavFor: ".b-slider__content-about",
  });
});

$(document).ready(function () {
  $(".b-slider__content-about").slick({
    dots: true,
    adaptiveHeight: true,
    infinite: false,
    speed: 1000,
    centerPadding: "50",
    cssEase: "linear",

    asNavFor: ".b-slider__image-block",
  });
});
